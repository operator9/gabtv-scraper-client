/*
 *  import
 *
 *  Copyright (c) 2021 operator9, swampcreature
 */

'use strict';

const time = require('./time');
const dbModule = require('./database');
const db = dbModule.getDB();

const insert = db.prepare('INSERT INTO gabtv VALUES (@category, @date, @datetext, @user, @name, @views, @id, @duration, @title, @url, @basetime, @updated)');
const update = db.prepare('UPDATE gabtv SET date = ?, datetext = ?, basetime = ? WHERE id = ?');
const insertDelta = db.prepare('INSERT INTO deltas VALUES (@date, @views)');

module.exports = function(path) {
  let dbs;
  try {
    dbs = require('better-sqlite3')(path);
  }
  catch(err) {
    return console.log(`Could not open source database.`);
  }

  const getAll = dbs.prepare('SELECT * FROM gabtv ORDER BY dateISO');
  const getDeltas = dbs.prepare('SELECT * FROM deltas ORDER BY date');
  const basetime = dbs.prepare('SELECT MAX(dateISO) basetime FROM gabtv').get().basetime;

  const insertDeltas = db.transaction(rows => {
    let count = 0;
    let existing = 0;

    for(const row of rows) {
      try {
        insertDelta.run(row);
      }
      catch {existing++;}

      process.stdout.write(`Progress deltas: ${ ++count } / ${ rows.length } (existing: ${ existing })\r`);
    }
  });

  const insertNew = db.transaction(rows => {
    let count = 0;
    let existing = 0;

    for(const row of rows) {
      const nRow = {
        id      : row.id,
        category: row.category,
        date    : row.dateISO, //time.toISO(new Date(row.dateISO)),
        datetext: row.date,
        user    : row.user,
        name    : null,
        views   : row.views,
        duration: row.time,
        title   : null,
        url     : row.url,
        basetime: null,
        updated: time.toISO(row.updated)
      };

      try {
        insert.run(nRow);
      }
      catch {
        if ( !row.date.includes('month') ) update.run(time.toISO(new Date(row.dateISO)), row.date, basetime, row.id);
        existing++;
      }

      process.stdout.write(`Progress records: ${ ++count } / ${ rows.length } (existing: ${ existing })\r`);
    }
  });

  insertDeltas(getDeltas.all());
  console.log();

  insertNew(getAll.all());
  console.log('Done.');

  dbs.close();
  db.close();
};
/*
 *  database handling
 *
 *  Copyright (c) 2021 operator9, swampcreature
 */

'use strict';

const fs = require('fs');
const Path = require('path');
const time = require('./time');

const today = time.getToday();
const yesterday = time.getYesterday();

const db = require('better-sqlite3')(global.configPath + '/data.sqlite');

db.prepare('CREATE TABLE IF NOT EXISTS gabtv (category, date, datetext, user, name, views int, id, duration real, title, url, basetime, updated, PRIMARY KEY("id"))').run();
db.prepare('CREATE TABLE IF NOT EXISTS deltas (date UNIQUE, views INTEGER, PRIMARY KEY("date"))').run();
db.prepare('CREATE TABLE IF NOT EXISTS history (date, id, diff int)').run();
db.prepare('CREATE UNIQUE INDEX IF NOT EXISTS "id" ON "gabtv" ("id")').run();
db.exec(getSQLViews());

db.prepare('CREATE TABLE IF NOT EXISTS pages (date int, category, count int)').run();
db.prepare('CREATE UNIQUE INDEX IF NOT EXISTS "date" ON "pages" ("date")').run();

const getViews = db.prepare('SELECT views FROM gabtv WHERE id = ?');
const insert = db.prepare('INSERT INTO gabtv VALUES( @category, @date, @datetext, @user, @name, @views, @id, @duration, @title, @url, @basetime, @updated )');
const update = db.prepare('UPDATE gabtv SET views = ?, updated = ? WHERE id = ?');
const getTotalViews = db.prepare('SELECT SUM(views) views FROM gabtv WHERE SUBSTR(date, 0, 11) < ?');
const addDelta = db.prepare('INSERT INTO deltas VALUES( @date, @views )');
const storeDiff = db.prepare('INSERT INTO history VALUES( @date, @id, @diff)');

const benfordViews = db.prepare('SELECT views FROM gabtv');

const learn = db.prepare('INSERT INTO pages VALUES( @date, @category, @count)');
const prediction = db.prepare('SELECT count FROM pages WHERE category = ? ORDER BY count DESC LIMIT 14');
const topCategories = db.prepare('SELECT category, count() count FROM gabtv GROUP BY category ORDER BY count DESC');

// get number of views before merge
const viewCount1 = getTotalViews.get(yesterday).views;

function addData(list) {
  const insertNew = db.transaction(rows => {
    for(const row of rows) {
      const exists = getViews.get(row.id);
      if ( exists ) {
        const maxViews = Math.max(exists.views, row.views);
        const diff = (row.views | 0) - (exists.views | 0);
        update.run(maxViews, time.getNowFull(), row.id);

        // store view diff history
        if ( diff ) storeDiff.run({ date: yesterday, id: row.id, diff });
      }
      else {
        try {
          insert.run(row);
        }
        catch(err) {
          console.log(`SQLite error: ${ err.message }`, row);
        }
      }
    }
  });

  insertNew(list);
};

function close(skipViews = false) {
  // update delta database
  if ( viewCount1 > 0 && !skipViews ) {
    const views = getTotalViews.get(today).views - viewCount1;
    const date = new Date(db.prepare('SELECT MAX(date) date FROM gabtv').get().date);
    date.setDate(date.getDate() - 1);

    try {
      addDelta.run({ date: time.toISOShort(date), views });
    }
    catch {console.log('Deltas already updated.');}
  }

  db.close();
}

function getSQLViews() {
  const sqlPath = Path.join(__dirname, '..', 'sql');
  const sql = [];

  try {
    fs.readdirSync(sqlPath).forEach(file => {
      const query = fs.readFileSync(Path.join(sqlPath, file), 'UTF8')
        .replace(/\r/g, '')
        .split('\n')
        .map((line, i) => line.split('--')[ 0 ])
        .join(' ')
        .replace(/\s+/g, ' ');

      const name = file.substr(0, file.length - 4);
      const drop = `DROP VIEW IF EXISTS "${ name }";`;
      sql.push(drop);
      const view = `CREATE VIEW "${ name }" AS ${ query };`;
      sql.push(view);
    });
  }
  catch(err) {console.log(err);}

  return sql.join('\n').replace(/(;;|;\s;|\s;)/g, ';');
}

function getPagePrediction(category) {
  const dataset = prediction.all(category).map(e => e.count);
  let count = 0;

  if ( dataset.length === 1 ) {
    count = dataset[ 0 ] + 1;
  }
  else if ( dataset.length > 1 ) {
    let diff = 0;
    for(let i = 0; i < dataset.length - 1; i++) {
      diff = +dataset[ i ] - dataset[ i + 1 ];
    }
    count = dataset[ 0 ] + Math.ceil(diff / (dataset.length - 1));
  }
  return count;
}

function pagesLearn(category, count) {
  learn.run({ date: Date.now(), category, count });
}

function getDailyViews(shortDate, max = 45) {
  max = Math.max(1, max | 0);
  return db
    .prepare('SELECT SUBSTR(date, 0, 11) day, SUM(views) views FROM gabtv WHERE SUBSTR(date, 0, 11) <= ? GROUP BY day ORDER BY date DESC LIMIT ?')
    .all(shortDate, max)
    .reverse();
}

// todo for future parallel processing
function getTopCategories() {
  return topCategories.all();
}

function getDB() {
  return db;
}

function getBenfordViews() {
  const { BenfordsLaw } = require('benfordslaw');
  const data = benfordViews.all().map(e => e.views);
  const benfords = new BenfordsLaw(data);
  return {
    chiSqr     : benfords.getChiSquared(),
    probability: benfords.getProbability()
  };
}

module.exports = { addData, close, pagesLearn, getPagePrediction, getTopCategories, getDB, getDailyViews, getBenfordViews };

/*
 *  getCategory
 *
 *  Copyright (c) 2021 operator9, swampcreature
 */

'use strict';

const getPage = require('./getPage');
const sleep = require('./sleep');

module.exports = async function getCategory(entry) {
  const pages = [];
  let page = 1;
  let ok = true;

  while( ok ) {
    try {
      const url = `https://tv.gab.com/category/${ entry.slug }/more?p=${ page }`;
      const html = await getPage(url);
      pages.push(html);

      if ( !html.trim().length ) ok = false;
      if ( ok ) {
        global.pbar.currentPage = page++;
      }

      await sleep(global.delay);
    }
    catch {ok = false;}
  }

  return {
    count: page,
    html : pages.join('')
  };
};
/*
 *  sleep
 *
 *  Copyright (c) 2021 operator9, swampcreature
 */
'use strict';

module.exports = async function sleep(ms) {
  return new Promise(resolve => {
    setTimeout(resolve, ms);
  });
};

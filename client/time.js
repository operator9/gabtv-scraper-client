/*
 *  time & date
 *
 *  Copyright (c) 2021 operator9, swampcreature
 */

'use strict';

const dayjs = require('dayjs');

const second = 1000;
const minute = second * 60;
const hour = minute * 60;
const day = hour * 24;
const week = day * 7;
const month = day * 30.42;

function _timeZ() {
  const d = new Date;
  d.setMinutes(d.getMinutes() - d.getTimezoneOffset());
  return d;
}

function getUnits() {
  return { second, minute, hour, day, week };
}

function getNowFull() {
  return toISO(_timeZ());
}

function getToday() {
  return toISOShort(_timeZ());
}

function getYesterday() {
  const d = _timeZ();
  d.setDate(d.getDate() - 1);
  return toISOShort(d);
}

function stamp2sec(txt) {
  const multipliers = [ 1, 60, 3600, 24 * 3600 ];
  return txt
    .split(':')
    .reverse()
    .map(v => v | 0)
    .reduce((n, v, i) => n + v * multipliers[ i ], 0);
}

function toISO(d = new Date) {
  return (typeof d === 'string' ? d : d.toISOString())
    .substr(0, 19)
    .replace('T', ' ');
}

function toISOShort(d = new Date) {
  return (typeof d === 'string' ? d : d.toISOString())
    .substr(0, 10);
}

function isoToDate(iso) {
  return (new dayjs(iso)).toDate();
}

function toDate(txt, base = new Date) {
  const d = new dayjs(base);
  txt = txt.toLowerCase().trim();
  const parts = txt.replace('few ', '').split(' ');
  const num = isNaN(parts[ 0 ]) ? 1 : +parts[ 0 ];
  return toISO(d.subtract(num, parts[ 1 ]).format());
}

module.exports = {
  getUnits, getNowFull, getToday, getYesterday, stamp2sec, toDate, toISO, toISOShort, isoToDate
};

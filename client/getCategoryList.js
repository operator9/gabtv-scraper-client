/*
 *  getCategories
 *
 *  Copyright (c) 2021 operator9, swampcreature
 */

'use strict';

const getPage = require('./getPage');

const regex = /href="([^\'\"]+)/g;

module.exports = async function getCategories(baseUrl) {
  const html = await getPage(baseUrl);
  const links = [];
  let m;

  while( (m = regex.exec(html)) !== null ) {
    if ( m.index === regex.lastIndex ) regex.lastIndex++;

    m.forEach((match, groupIndex) => {
      if ( groupIndex === 1 && match.startsWith('/category/') ) links.push(match);
    });
  }

  return links.map(link => {
    const slug = link.split('/')[ 2 ];
    return {
      url : `${ baseUrl }/${ slug }`,
      path: link,
      slug
    };
  });
};
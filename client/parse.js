/*
 *  parse html string into virtual DOM
 *
 *  Copyright (c) 2021 operator9, swampcreature
 */

'use strict';

const htmlparser = require('htmlparser2');
const { getNowFull, stamp2sec, toDate } = require('./time');

const attributes = [
  'data-episode-id',
  'data-episode-url',
  'title',
  'class' // -> duration-label
];

module.exports = function parse(category, html) {
  return new Promise((resolve, reject) => {

    const updated = getNowFull();
    const baseDate = new Date(updated);
    const list = [];

    let o = {};
    let duration = false;
    let spans = 0;
    let lastText = '';

    const parser = new htmlparser.Parser({
      onattribute(name, value) {
        const index = attributes.indexOf(name);

        if ( index === 0 ) {
          // start new video parse
          duration = false;
          spans = 0;
          lastText = '';
          o = { category, updated };
          o.id = value;
        }
        else if ( index === 1 && !o.url ) {
          const parts = value.split('/');
          o.user = parts[ 2 ];
          o.url = parts[ 4 ];
        }
        else if ( index === 2 && !o.title ) {
          o.title = value;
        }
        else if ( index === 3 && !duration ) {
          if ( value.split(' ').includes('duration-label') ) {
            duration = true;
          }
        }
      },
      onopentag(name) {
        if ( name === 'span' ) {
          if ( !spans++ ) o.name = lastText;
        }
      },
      ontext(text) {
        lastText = text;
        if ( duration ) {
          o.duration = stamp2sec(text);
          duration = false;
        }
        else if ( spans ) {
          if ( spans === 1 ) {
            o.views = toViews(text);
          }
          else if ( spans === 4 ) {
            o.datetext = text;
            o.date = toDate(text, baseDate);
            o.basetime = updated;
          }
        }
      },
      onclosetag(tagname) {
        if ( tagname === 'span' && spans > 3 ) {
          if ( checkObject(o) ) list.push(o);
          spans = 0;
        }
      },
      onend() {
        resolve(list);
      },
      onerror(error) {
        reject(error);
      }
    });

    parser.write(html);
    parser.end();
  });
};

function checkObject(o) {
  for(let key of Object.keys(o)) {
    if ( typeof o[ key ] === 'undefined' || o[ key ] === null ) {
      console.warn('WARNING: Object is missing properties. HTML structure may have changed', o);
      return false;
    }
    return true;
  }
}

function toViews(txt) {
  let n;
  txt = txt.trim().toLowerCase();
  if ( txt.endsWith('k') ) {
    const parts = txt.split('.').map(p => p.replace(/[^0-9]+/, '') | 0);
    parts[ 1 ] |= 0; // force numeric value in index 1
    n = (parts[ 0 ] + (parts[ 1 ] / Math.pow(10, parts[ 1 ].toString().length))) * 1000;
  }
  else if ( txt.endsWith('m') ) {
    const parts = txt.split('.').map(p => p.replace(/[^0-9]+/, '') | 0);
    parts[ 1 ] |= 0;
    n = (parts[ 0 ] + (parts[ 1 ] / Math.pow(10, parts[ 1 ].toString().length))) * 1e6;
  }
  else {
    n = txt.replace(/[^0-9]+/, '');
  }
  return n | 0;
}

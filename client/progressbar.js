/*
 *  progressbar
 *
 *  Copyright (c) 2021 operator9, swampcreature
 */

'use strict';

const width1 = 20;
const width2 = 40;
//const open = '\ue0b2';
//const close = '\ue0b0';

let i = 0;
const chars = [ '/', '-', '\\', '|' ];

module.exports = function render() {
  const pbar = global.pbar;
  const learning = '... learning ...';
  const pad = ''.padEnd((width2 - learning.length) >>> 1);
  const learnText = (pad + learning + pad).split('');

  learnText[ i++ % learnText.length ] = `\x1b[32m${ chars[ i % chars.length ] }\x1b[34m`;

  const prefix = `\x1b[37;1m${ pbar.category.padStart(16, ' ') }\x1b[m`;

  const c = Math.min(width1, pbar.total ? Math.round((pbar.current / pbar.total) * width1) : 0);
  const p = Math.min(width2, pbar.totalPages ? Math.round((pbar.currentPage / pbar.totalPages) * width2) : 0);

  const catBar = `\x1b[43m${ ''.padEnd(c) }\x1b[m${ ''.padEnd(width1 - c, '░') }\x1b[m`;
  const pageBar = pbar.totalPages
                  ? `\x1b[44m${ ''.padEnd(p) }\x1b[m${ ''.padEnd(width2 - p, '░') }\x1b[m`
                  : `\x1b[31m[\x1b[34m${ learnText.join('') }\x1b[31m]\x1b[m`;

  process.stdout.write(`${ prefix } ${ catBar } ${ pageBar }\r`);
};
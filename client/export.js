/*
 *  export
 *
 *  Copyright (c) 2021 operator9, swampcreature
 */

'use strict';

const fs = require('fs');
const db = require('better-sqlite3')(global.configPath + '/data.sqlite', { readonly: true });

function exportCSV(file) {
  try {
    const rows = db.prepare('SELECT * FROM gabtv ORDER BY date').all();
    if ( rows.length ) {
      const lines = [];
      let header = false;

      rows.forEach(row => {
        if ( !header ) {
          lines.push(Object.keys(row).join(';'));
          header = true;
        }
        lines.push(Object.values(row).join(';'));
      });

      fs.writeFileSync(file, lines.join('\r\n'), 'UTF8');
      console.log('Done.');
    }
    else console.log('Nothing to export.');
  }
  catch(err) { console.log('Nothing to export.');}

  db.close();
}

module.exports = { exportCSV };
/*
 *  cache handling
 *
 *  Copyright (c) 2021 operator9, swampcreature
 */

'use strict';

const Path = require('path');
const fs = require('fs');
const crypto = require('crypto');

const tmpFolder = '/tmp/';
const tmpPrefix = 'gabtv_';

function clearCache() {
  fs.readdirSync(tmpFolder)
    .filter(filename => filename.startsWith(tmpPrefix))
    .forEach(filename => fs.unlinkSync(Path.join(tmpFolder, filename)));
}

function getCacheFilename(data) {
  return Path.join(tmpFolder, tmpPrefix + crypto.createHash('md5').update(data).digest('hex'));
}

function writeCache(id, data) {
  const filename = getCacheFilename(id);
  try {
    fs.writeFileSync(filename, data, 'utf-8');
  }
  catch {return null;}
  return filename;
}

function readCache(id) {
  const filename = getCacheFilename(id);
  try {
    return fs.readFileSync(filename, 'utf-8');
  }
  catch {}
  return null;
}

module.exports = { clearCache, getCacheFilename, writeCache, readCache };
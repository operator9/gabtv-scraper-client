Gab TV Client Scraper
=====================

Standalone client collecting public data from Gab TV for statistical purposes.

The script is intended for researchers.

This repo is not associated with Gab, nor is it part of any affiliation.

NOTE: Data deviates slightly from the browser based due to timezone changes. You can build the
initial base data from existing incremental browser databases.


Features
--------

- Scrape GabTV site (no need to log in) for metadata of available videos in categories (currently 6
  months back from and including current month)
- Data stored in SQLite database
- Export to CSV
- Various data analytics queries embedded in the SQLite database (as views)
- Produces PNG, JPEG, SVG or PDF reports from the scraped data

Instructions
------------

The scraper is developed primarily to run on Linux systems, but you should be able to it on other
systems such as Windows and Mac. Only tested with Linux.

Make sure to have Node.js v14 or newer properly installed (it may work with older versions, but we
haven't tested for this.) Node comes bundled with NPM. For producing reports make sure you have the
prerequisites installed for node-canvas (
see [system specific install instructions](https://github.com/Automattic/node-canvas/wiki/_pages).)

Clone the repository:

    $ git clone gabtv-scraper-client

CD into project folder and install dependencies:

    $ npm i

Optionally (recommended) install as a global command:

    $ npm i -g .

With the latter you can run the command `gabtvscraper` from anywhere using the terminal. Otherwise,
run `node .` while in the project folder.

Prepare first run
-----------------

You can start from scratch, or copy the included primed database in
`local-share-gabtvscrasper` to your `$HOME/.local/share/gabtvscraper` folder (
or `$HOME\gabtvscraper` on Windows).

Using the primed database will give you some older data no longer available (Gab arbitrarily limits
browsing to 6 months).

Daily cron
----------

To run daily add it to a cron job. You may use a non-sudo user for this.

    $ crontab -e

Add the following lines

    # Run daily at midnight
    0 0 * * * gabtvscraper --clear-cache >/dev/null 2>&1
    0 0 * * * sleep 10 && gabtvscraper >/dev/null 2>&1

Scraping
--------

To capture more accurate timestamps and view counts the script need to run daily, ideally at the
same time and as close to Gab's midnight (EST).

Note that the timezone you will be using will affect the numbers within a day, but the numbers are
correct in sum. The purpose is longer term analysis.

The scraper collects data points from each category page which currently includes:

- Video ID (used to remove duplicates)
- User ID (who owns the video)
- "date" - there is no real timestamp so for example "2 hours ago" is translated into an approximate
  timestamp.
- Number of views for that video (not locked to date, see separate `history` table)
- Category
- View time in seconds
- URL-slug to video
- Basetime (your local time from which posting time offset is calculated)
- Last updated time

A full scraping is currently taking about 30 minutes depending on various factors.

Usage
-----

Simply run `gabtvscraper` to start scraping (or in the project folder: `node .`).

To generate graphs (pdf, png, svg or jpeg):

    $ gabtvscraper --graph pdf output-folder
    $ gabtvscraper -g png output-folder

To export as CSV file:

    $ gabtvscraper --export file.csv
    $ gabtvscraper -x file.csv

See `--help` for more options.

Data Quality and Caveats
------------------------

Data quality is suffering primarily due to the lack of accurate timestamps. However, the data is
more than adequate for analysing short and long term trends.

The quality can be increased by running the scraper more often, i.e. several times per day to
capture more accurate timestamps for when a video was posted.

Another issue is that data is limited to 6 months. It's possible to work around this by scraping
each individual videos older than 6 months. But due to a severe fault on Gabs view count handling
each such lookup will itself produce a "view"
for that video and over time add bias to the overall result.

Timezone used for scraping will affect numbers for a single day, but numbers will add up correctly
in sum. We recommend scraping with timezone set to EST as this appear to be what Gab is using for
their internal data. Using CST shouldn't affect the daily data significantly.

Modifying the Code
------------------

New sql query files added to the `sql/` folder are dynamically updated for each run. Make sure to
update `client/graphs.js` to produce graphs from it. There is no documentation here at the moment as
it's constantly subject to change, but looking at existing reports should give useful hints.

Any other changes to the code are instant, including the globally installed run file (sym-linked) if
you opted for that.

How To Read And Query SQLite Databases
--------------------------------------

Install the free open source tool "DB Browser for SQLite". It's available for Windows, Mac and
Linux.

https://sqlitebrowser.org/

After installation, open the scraper database from it and run any SQL query you like in the 'Execute
SQL' tab. The scraper database includes several predefined queries as views. The views can be
browsed in the 'Browse Data' tab. NOTE: These views may change over time.

Support
-------

The code is provided AS-IS.

TODO
----

- Distributed scan mode for speed increase and in case of counter-measures

License
-------

MIT (c) 2021 Operator9, swampcreature

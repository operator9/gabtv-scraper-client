SELECT
	name || ' (' || user || ')' as user,
	SUM(views) views
FROM
	gabtv
GROUP BY
	user
ORDER BY
	views DESC
LIMIT 20
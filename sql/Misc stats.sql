SELECT
  COUNT(DISTINCT user) "Total # of posters",
  COUNT() "Total # of videos",
  SUM(views) "Total # of views",
  ROUND(AVG(views), 1) "Avg views/video",
  (SELECT ROUND(AVG(VIEWS),1) FROM gabtv WHERE views < 1500) "Avg views bias adj", -- est. bias cut-off
  MAX(views) "Max views",
  ROUND(COUNT() / 180, 0) "Avg # videos/day",
  ROUND(COUNT() / COUNT(DISTINCT user), 1) "Avg # videos/poster",
  ROUND(AVG(duration) / 60, 1) "Avg playtime (min)",
  MAX(duration) / 60 "Longest video (min)",
  MIN(duration) "Shortest video (sec)",
  (SELECT COUNT() FROM gabtv WHERE views > 1000) "Videos with > 1K views",
  (SELECT COUNT() FROM gabtv WHERE views > 10000) "Videos with > 10K views",
  (SELECT COUNT() FROM gabtv WHERE views = 0) "Videos with no views",
  (SELECT COUNT() FROM gabtv WHERE views < 10) "Videos with < 10 views",
  (SELECT COUNT(*) FROM gabtv WHERE duration < 30) "# of videos < 30s"

FROM gabtv

SELECT
	name || ' (' || user || ')' as user,
	COUNT(views) videos
FROM
	gabtv
GROUP BY
	user
ORDER BY
	videos DESC
LIMIT 20
#!/usr/bin/env node

/*
 *  Client for Gab TV scraper
 *  v1.0.0-alpha
 *
 *  Copyright (c) 2021 operator9, swampcreature
 */

'use strict';

global.DEBUG = false;
global.delay = 75;  // ms

const os = require('os');
const Path = require('path');
const mkdirp = require('mkdirp');

global.cachePrefix = Path.resolve(os.tmpdir()) + Path.sep + 'gabtv_';

const configPath = Path.join(os.homedir(), process.platform === 'win32' ? 'gabtvscraper' : '.local/share/gabtvscraper');
global.configPath = configPath;

const baseUrl = 'https://tv.gab.com/category';

const options = require('commander')
  .option('-g, --graphs <type>', 'Generate graph file as pdf (single file), svgs, pngs or jpegs.')
  .option('-x, --export <file.csv>', 'Export data as CSV file.')
  .option('-i, --import <path>', 'Import an existing base browser generated database.')
  .option('-b, --benfords', 'Show Benford\'s Law numbers for view numbers.')
  //.option('--fix-deltas <dbfolder>', 'Calculate difference in view counts based on a set of daily databases.')
  //.option('--fix-times', 'Re-calculate times from basetime.')
  .option('--clear-cache', 'Delete all cached files from temp folder.')
  .parse(process.argv);

const args = options.opts();

if ( args.benfords ) {
  const db = require('./client/database');
  const result = db.getBenfordViews();
  console.log(`Chi²       : ${ result.chiSqr } ( lower is better )`);
  console.log(`Probability: ${ result.probability?.toFixed(1) } ( >= 0.9 )`);
  return;
}
else if ( args.export ) {
  console.log(`'Exporting data to "${ args.export }".`);
  const { exportCSV } = require('./client/export');
  exportCSV(args.export);
  return;
}
else if ( args.import ) {
  console.log(`Importing data from "${ args.import }".`);
  require('./client/import')(args.import);
  return;
}
else if ( args.graphs ) {
  console.log(`Generating graphs in "${ options.args[ 0 ] }".`);
  (async function() {
    await require('./client/graphs')(options.args[ 0 ], args.graphs);
  })();
  return;
}
  //else if ( args.fixDeltas ) {
  //  console.log(`Recalculating view deltas from "${ args.fixDeltas }".`);
  //  require('./client/fixdeltas')(args.fixDeltas);
  //  return;
  //}
  //else if ( args.fixTimes ) {
  //  console.log('Recalculating time offsets from basetime.');
  //  require('./client/fixtimes')();
  //  return;
//}
else if ( args.clearCache ) {
  console.log(`Deleting cached files.`);
  const { clearCache } = require('./client/cache');
  clearCache();
  return;
}

// MAIN

const getCategoryList = require('./client/getCategoryList');
const getCategory = require('./client/getCategory');
const parse = require('./client/parse');
const { addData, close, pagesLearn, getPagePrediction } = require('./client/database');
const progressbar = require('./client/progressbar');

mkdirp(configPath);

const pbar = {
  category   : '',
  current    : 0,
  total      : 0,
  currentPage: 0,
  totalPages : 0
};

global.pbar = pbar;

let refPbar;

// start scraping
getCategoryList(baseUrl)
  .then(categories => {

    // TODO this process will be changed to round-robin method and use
    // several parallel requests to speed up processing, and to enable
    // multiple clients running from different locations if need be.
    // Currently, the need is not precarious, but is continuously evaluated.

    const timeStart = Date.now();
    let count = 0;

    if ( global.DEBUG ) categories = [ categories[ 1 ] ];

    refPbar = setInterval(progressbar, 500);
    console.log('\x1b[2J\x1b[;HScraping Gab TV meta-data...');

    (async function next() {
      if ( count < categories.length ) {
        const category = categories[ count++ ];

        pbar.category = category.slug;
        pbar.current = count;
        pbar.total = categories.length;

        pbar.totalPages = getPagePrediction(category.slug);
        pbar.currentPage = 0;

        const result = await getCategory(category);
        const list = await parse(category.slug, result.html);

        addData(list);
        pagesLearn(category.slug, result.count);

        setImmediate(next);
      }
      else {
        clearInterval(refPbar);
        close();
        const timeEnd = (Date.now() - timeStart) / 60000;

        console.log('\x1b[2J\x1b[;H\x1b[mCompleted.');
        console.log(`Finished in ${ timeEnd.toFixed(1) } minutes.`);
      }
    })();

  })
  .catch(err => console.error(err));
